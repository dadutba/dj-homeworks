from django.db import models
import random

START = 0
END = 10

def get_random_int():
    return random.randint(START, END)


class Player(models.Model):
    
    class Meta:
        verbose_name = 'Игрок'
        verbose_name_plural = 'Игроки'

    def __str__(self):
        return f'{self.id}'


class Game(models.Model):
    creator = models.ForeignKey(Player, on_delete=models.CASCADE, verbose_name='Создатель')
    value = models.PositiveSmallIntegerField(default=get_random_int, verbose_name='Загадано')
    active = models.BooleanField(default=True, verbose_name='Активна')

    class Meta:
        verbose_name = 'Игра'
        verbose_name_plural = 'Игры'

    def __str__(self):
        return f'{self.id}'


class PlayerGameInfo(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE, verbose_name='Игрок')
    game = models.ForeignKey(Game, on_delete=models.CASCADE, verbose_name='Игра')
    attempts = models.PositiveIntegerField(default=0, verbose_name='Число попыток')
    attempted_value = models.PositiveSmallIntegerField(default=None, null=True, verbose_name='Последнее значение')
    gessed = models.BooleanField(default=False, verbose_name='Угадана')

    class Meta:
        verbose_name = 'Данные игра-игрок'
        unique_together = ['player', 'game']

    def __str__(self):
        return f'{self.game}__{self.player}'

