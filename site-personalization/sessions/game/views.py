from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.shortcuts import redirect, render
# from django.views.defaults import server_error


from .forms import GessForm
from .models import Player, Game, PlayerGameInfo    


def show_home(request):

    id = request.session.get('playerid')
    if id is None:
        player = Player.objects.create()
        request.session['playerid'] = id = player.id
    else:
        try:
            player = Player.objects.get(id=id)
        except Player.DoesNotExist:
            # return server_error()
            HttpResponseBadRequest('Неверный playerid')

    game, _ = Game.objects.get_or_create(
        active=True,
        defaults=dict(creator=player)
    )

    try:
        gessed_game_info = PlayerGameInfo.objects.get(game=game, gessed=True)
        gessed = True
        attempts = gessed_game_info.attempts
        gessed_by = gessed_game_info.player
    except PlayerGameInfo.DoesNotExist:
        gessed_game_info = None
        gessed = False
        attempts = 0
        gessed_by = None

    current_game_info, _ = PlayerGameInfo.objects.get_or_create(game=game, player=player)

    if request.method == 'POST':
        try:
            attempted_value = request.POST.get('value')
            attempted_value = int(attempted_value)
            current_game_info.attempts += 1
            current_game_info.attempted_value = attempted_value
            if not gessed and attempted_value == game.value:
                current_game_info.gessed = True
            current_game_info.save()
        except ValueError:
            pass
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    attempted_value = current_game_info.attempted_value
    was_attempt = current_game_info.attempts > 0

    is_creator = player == game.creator
    is_gesser = gessed_by == player

    if gessed and is_creator:
        game.active = False
        game.save()

    report_string = ''
    if attempted_value is not None and not gessed:
        if attempted_value < game.value:
            report_string = f'Загаданное число больше {attempted_value}'
        elif attempted_value > game.value:
            report_string = f'Загаданное число меньше {attempted_value}'
        

    return render(
        request,
        'home.html',
        context=dict(
            is_creator=is_creator,
            is_gesser=is_gesser,
            value=game.value,
            gessed=gessed,
            attempts=attempts,
            gessed_by=gessed_by,
            was_attempt=was_attempt,
            report_string=report_string,
            form=GessForm()
        )
    )

