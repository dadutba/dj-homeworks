from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import Article, Profile


def show_articles(request):
    return render(
        request,
        'articles.html',
        { 'articles': Article.objects.all() }
    )


def show_article(request, id):
    return render(
        request,
        'article.html',
        { 'article': Article.objects.get(id=id) }
    )

def subscribe(request, state):
    request.user.profile.has_subscription = bool(state)
    request.user.profile.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
