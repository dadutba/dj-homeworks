from django.contrib import admin

from .models import Car, Review
from .forms import ReviewAdminForm

class CarAdmin(admin.ModelAdmin):
    def review_count(self, obj):
        return obj.review_set.count()

    review_count.short_description = 'Число обзоров'

    list_display = ('brand', 'model', 'review_count')
    search_fields = ('brand', 'model')
    list_filter = ('brand', 'model')
    ordering = ('-brand',)


class ReviewAdmin(admin.ModelAdmin):
    form = ReviewAdminForm
    list_display = ('car', 'title')
    search_fields = ('car', 'title')
    list_filter = ('car',)
    ordering = ('id',)


admin.site.register(Car, CarAdmin)
admin.site.register(Review, ReviewAdmin)
