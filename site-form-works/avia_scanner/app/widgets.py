from django.forms import DateTimeInput
from django.forms.widgets import TextInput


class AjaxInputWidget(TextInput):
    template_name = 'widget/ajax_input_widget.html'
    url = ''

    def __init__(self, url, attrs=None):
        """url: путь к ajax API которое будет возвращать список городов для подстановки"""
        super().__init__(attrs)
        self.url = url

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['url'] = self.url
        return context

class BootstrapDateTimePickerInput(DateTimeInput):
    template_name = 'widget/bootstrap_datetimepicker.html'

    def __init__(self, attrs=None, width='300px', input_format='%d/%m/%Y'):
        super().__init__(attrs)
        self.input_format = input_format
        self.width = width

    def get_context(self, name, value, attrs):
        datetimepicker_id = f'datetimepicker_{name}'
        if attrs is None:
            attrs = dict()
        attrs['data-target'] = f'#{datetimepicker_id}'
        attrs['class'] = attrs.get('class', '') + ' form-control datetimepicker-input'
        context = super().get_context(name, value, attrs)
        context['widget']['datetimepicker_id'] = datetimepicker_id
        context['widget']['width'] = self.width
        context['widget']['input_formats'] = self.input_format
        return context
