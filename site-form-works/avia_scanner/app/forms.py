from django import forms
from django.urls import reverse_lazy

from .widgets import AjaxInputWidget, BootstrapDateTimePickerInput
from .models import City

FROM_CHOICES = (
    (0, '------'),
    (1, 'Москва')
)

class SearchTicket(forms.Form):
    from_city = forms.ChoiceField(label='Город отправления', choices=FROM_CHOICES, widget=forms.Select(attrs={'style': 'width:200px'}))
    to_city = forms.CharField(label='Город прибытия', widget=AjaxInputWidget(reverse_lazy('cities-lookup'), attrs={'class': 'inline right-margin'}))
    # date = forms.DateField(widget=forms.SelectDateWidget())
    date = forms.DateTimeField(widget=BootstrapDateTimePickerInput(attrs={'style': 'width:200px;'}, input_format='DD/MM/YYYY'))
