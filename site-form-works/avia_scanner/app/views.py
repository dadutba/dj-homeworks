import time
import random

from django.views.generic import TemplateView
from django.views.generic.edit import FormMixin
from django.http import JsonResponse
from django.core.cache import cache

from .models import City
from .forms import SearchTicket

CITIES_CACHE_TIMEOUT = 500


class TicketPageView(FormMixin, TemplateView):
    form_class = SearchTicket
    template_name = 'app/ticket_page.html'


def cities_lookup(request):
    """Ajax request предлагающий города для автоподстановки, возвращает JSON"""
    cities = cache.get('to_cities')
    if not cities:
        cities = list(City.objects.values_list('name', flat=True).order_by('name'))
        cache.set('to_cities', cities, timeout=CITIES_CACHE_TIMEOUT)
    term = request.GET.get('term')
    results = [city for city in cities if city.startswith(term)]
    return JsonResponse(results, safe=False)
