from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import ListView, DetailView

from .models import Product, Review
from .forms import ReviewForm


class ProductsList(ListView):
    model = Product
    context_object_name = 'product_list'


class ProductView(DetailView):
    model = Product

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        context['form'] = ReviewForm()
        context['reviews'] = Review.objects.filter(product=self.object)
        context['is_review_exist'] = self.object.id in self.request.session.get('reviewed_products', [])
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.id not in request.session.get('reviewed_products', []):
            form = ReviewForm(request.POST).save(commit=False)
            form.product = self.object
            form.save()
            request.session.setdefault('reviewed_products', []).append(self.object.id)
            request.session.modified = True

        return redirect(request.path_info)
