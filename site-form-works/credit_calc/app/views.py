from django.views.generic import TemplateView

from .forms import CalcForm

# INITIAL_FORM_VALUES = {
#     'initial_fee': 100,
#     'rate': 10,
#     'months_count': 12
# }


class CalcView(TemplateView):
    template_name = 'app/calc.html'
    
    def get_context_data(self, **kwargs):
        context = super(CalcView, self).get_context_data(**kwargs)
        # context['form'] = CalcForm(self.request.GET or None, initial=INITIAL_FORM_VALUES)
        context['form'] = CalcForm(self.request.GET or None)
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        form = context['form']
        if form.is_valid():
            context.update(form.calculate())

        return super(TemplateView, self).render_to_response(context)

