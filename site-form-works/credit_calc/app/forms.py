from django import forms


class CalcForm(forms.Form):
    initial_fee = forms.IntegerField(label="Стоимость товара", initial=100)
    # rate = forms.CharField(label="Процентная ставка")
    rate = forms.IntegerField(label="Процентная ставка", initial=10)
    months_count = forms.IntegerField(label="Срок кредита в месяцах", initial=10)

    def clean_initial_fee(self):
        # валидация одного поля, функция начинающаяся на `clean_` + имя поля
        initial_fee = self.cleaned_data.get('initial_fee')
        if not initial_fee or initial_fee < 0:
            raise forms.ValidationError("Стоимость товара не может быть отрицательной")
        return initial_fee

    def clean_months_count(self):
        months_count = self.cleaned_data.get('months_count')
        if not months_count or months_count < 0:
            raise forms.ValidationError("Стоимость товара не может быть отрицательной")
        return months_count

    def clean_rate(self):
        rate = self.cleaned_data.get('rate')
        if not rate or rate < 0:
            raise forms.ValidationError("Процентная ставка не может быть отрицательной")
        return rate

    # def clean_rate(self):
    #     rate = self.cleaned_data.get('rate')
    #     try:
    #         rate = int(rate)
    #     except ValueError:
    #         raise forms.ValidationError("Процентная ставка должна быть числом")
    #     if not rate or rate < 0:
    #         raise forms.ValidationError("Процентная ставка не может быть отрицательной")
    #     return rate

    def clean(self):
        # общая функция валидации
        return self.cleaned_data

    def calculate(self):
        initial_fee = self.cleaned_data.get('initial_fee')
        rate = self.cleaned_data.get('rate')
        months_count = self.cleaned_data.get('months_count')
        common_result = initial_fee * (1 + rate / 100)
        month_result = common_result / months_count
        return {
            'common_result': round(common_result, 2),
            'result': round(month_result, 2)
        }

