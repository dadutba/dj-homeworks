"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from datetime import datetime
from django.contrib import admin
from django.urls import path, register_converter
from books.views import BookListView

class DatePathConverter:
    regex = '\d\d\d\d-\d\d-\d\d'

    def to_python(self, date_str):
        date = datetime.strptime(date_str, "%Y-%m-%d").date()
        return date

    def to_url(self, date):
        return date.strftime("%Y-%m-%d")

register_converter(DatePathConverter, 'date')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('books/', BookListView.as_view(), name='books'),
    path('books/<date:date>/', BookListView.as_view(), name='books')
]
