from datetime import datetime

from django.views import generic

from .models import Book

class BookListView(generic.ListView):
    template_name = 'index.html'
    model = Book
    context_object_name = 'books'

    def get_queryset(self):
        queryset = super(BookListView, self).get_queryset()
        date = self.kwargs.get('date')
        if date:
            queryset = queryset.filter(pub_date=date)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(BookListView, self).get_context_data()

        date = None
        try: 
            date = self.kwargs.get('date').strftime("%Y-%m-%d")
        except (ValueError, AttributeError):
            pass

        if date:
            previous = next =  None

            try:
                previous = Book.objects.values_list('pub_date', flat=True).order_by('-pub_date').filter(pub_date__lt=date)[0].strftime("%Y-%m-%d")
            except (ValueError, IndexError, AttributeError):
                pass

            try:
                next = Book.objects.values_list('pub_date', flat=True).order_by('pub_date').filter(pub_date__gt=date)[0].strftime("%Y-%m-%d")
            except (ValueError, IndexError, AttributeError):
                pass

            context['date_page'] = { 'previous': previous, 'current': date, 'next': next }
            context['is_date_paginated'] = True

        return context

