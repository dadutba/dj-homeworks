from django.contrib import admin

from .models import Phone, Extended

# admin.site.register(Phone)
# admin.site.register(Extended)

class ExtendedInline(admin.StackedInline):
    model = Extended
    extra = 0

@admin.register(Phone)
class PhoneAdmin(admin.ModelAdmin):
    inlines = [ExtendedInline]
