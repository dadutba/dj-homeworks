from django.db import models

class Phone(models.Model):
    name = models.CharField(max_length=40)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    os = models.CharField(max_length=40)
    ram = models.SmallIntegerField()
    ppd = models.SmallIntegerField()
    double_camera = models.BooleanField()
    proc = models.CharField(max_length=60)
    resolution = models.CharField(max_length=40)
    fm_radio = models.BooleanField()
    def __str__(self):
        return self.name

class Extended(models.Model):
    phone = models.ForeignKey(Phone, on_delete=models.DO_NOTHING)
    value = models.CharField(max_length=60)
