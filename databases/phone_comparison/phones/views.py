from django.shortcuts import render

from .models import Phone, Extended

def show_catalog(request):
    phones = Phone.objects.all().prefetch_related('extended_set')
    return render(
        request,
        'catalog.html',
        { 'phones': phones }
)
