from django.shortcuts import render

from .models import Phone

def show_catalog(request):
    sort_field = request.GET.get('sort')
    if sort_field == 'min_price':
        sort_field = 'price'
    elif sort_field == 'max_price':
        sort_field = '-price'
    else:
        sort_field = 'name'
    return render(
        request,
        'catalog.html',
        {'phones': Phone.objects.all().order_by(sort_field)}
    )


def show_product(request, slug):
    return render(
        request,
        'product.html',
        {'phone': Phone.objects.get(slug=slug)}
    )
