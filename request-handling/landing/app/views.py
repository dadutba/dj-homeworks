from collections import Counter

from django.http import Http404
from django.shortcuts import render_to_response

# Для отладки механизма ab-тестирования используйте эти счетчики
# в качестве хранилища количества показов и количества переходов.
# но помните, что в реальных проектах так не стоит делать
# так как при перезапуске приложения они обнулятся
counter_show = Counter()
counter_click = Counter()

LANDING_TEMPLATES = {
    'original': 'landing.html',
    'test': 'landing_alternate.html'
}


def index(request):
    # Реализуйте логику подсчета количества переходов с лендига по GET параметру from-landing
    from_landing = request.GET.get('from-landing')
    if from_landing in LANDING_TEMPLATES.keys():
        counter_click[from_landing] += 1
    return render_to_response('index.html')


def landing(request):
    # Реализуйте дополнительное отображение по шаблону app/landing_alternate.html
    # в зависимости от GET параметра ab-test-arg
    # который может принимать значения original и test
    # Так же реализуйте логику подсчета количества показов
    ab_test_arg = request.GET.get('ab-test-arg')
    if ab_test_arg in LANDING_TEMPLATES.keys():
        counter_show[ab_test_arg] += 1
        return render_to_response(LANDING_TEMPLATES[ab_test_arg])
    raise Http404()


def stats(request):
    # Реализуйте логику подсчета отношения количества переходов к количеству показов страницы
    # Чтобы отличить с какой версии лендинга был переход
    # проверяйте GET параметр marker который может принимать значения test и original
    # Для вывода результат передайте в следующем формате:

    marker = request.GET.get('marker')
    if marker in LANDING_TEMPLATES.keys():
        counter_click[marker] += 1

    return render_to_response('stats.html', context={
        'test_conversion': counter_click['test']/counter_show['test'] if counter_show['test'] else 0.0,
        'original_conversion': counter_click['original']/counter_show['original'] if counter_show['original'] else 0.0,
    })
