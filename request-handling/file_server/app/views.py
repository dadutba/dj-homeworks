import datetime

from django.conf import settings
from django.http import Http404
from django.shortcuts import render
from django.views.generic import TemplateView

from os import stat, listdir
from os.path import join, isfile

fromtimestamp = datetime.datetime.fromtimestamp

class FileList(TemplateView):
    template_name = 'index.html'
    
    def get_context_data(self, date=None):
        path = settings.FILES_PATH
        files = []
        for file in listdir(path):
            info = stat(join(path, file))
            ctime = fromtimestamp(info.st_ctime)
            if date is not None and ctime.date() != date:
                continue
            files.append({
                'name': file,
                'ctime': ctime,
                'mtime': fromtimestamp(info.st_mtime)
            })

        return {
            'files': files,
            'date': date
        }


def get_file_content(filepath):
    with open(filepath) as f:
        return f.read()


def file_content(request, name):
    filepath = join(settings.FILES_PATH, name)
    if not isfile(filepath):
        raise Http404()

    return render(
        request,
        'file_content.html',
        context={'file_name': filepath, 'file_content': get_file_content(filepath)}
    )

