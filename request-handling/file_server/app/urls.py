from datetime import datetime

from django.urls import path, register_converter

from .views import FileList, file_content

# Определите и зарегистрируйте конвертер для определения даты в урлах и наоборот урла по датам
class DatePathConverter:
    regex = '\d\d\d\d-\d\d-\d\d'

    def to_python(self, date_str):
        date = datetime.strptime(date_str, "%Y-%m-%d").date()
        return date

    def to_url(self, date):
        return date.strftime("%Y-%m-%d")

register_converter(DatePathConverter, 'date')


urlpatterns = [
    # Определите схему урлов с привязкой к отображениям .views.FileList и .views.file_content
    # path(..., name='file_list'),
    # path(..., name='file_list'),
    # path(..., name='file_content'),

    path('', FileList.as_view(), name='file_list'),
    path('<date:date>', FileList.as_view(), name='file_list'),
    path('<str:name>', file_content, name='file_content'),
]
