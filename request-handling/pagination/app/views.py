import csv

from django.core.paginator import Paginator
from django.conf import settings
from django.shortcuts import render_to_response, redirect
from django.urls import reverse

from urllib.parse import urlencode

ITEMS_PER_PAGE = 25

def index(request):
    response = redirect(reverse(bus_stations))
    response['Location'] += '?' + urlencode(request.GET.dict())
    return response


def bus_stations(request):

    paginator = Paginator([], ITEMS_PER_PAGE)
    with open(settings.BUS_STATION_CSV, encoding='cp1251') as f:
        reader = csv.DictReader(f)
        paginator = Paginator(list(reader), ITEMS_PER_PAGE)

    page = request.GET.get('page')
    page_info = paginator.get_page(page)

    pars = dict(request.GET.dict())

    prev_url = None
    if page_info.has_previous():  
        pars.update({'page': page_info.previous_page_number()})
        prev_url = 'bus_stations?' + urlencode(pars)

    next_url = None
    if page_info.has_next():
        pars.update({'page': page_info.next_page_number()})
        next_url = 'bus_stations?' + urlencode(pars)


    return render_to_response('index.html', context={
        'bus_stations': page_info,
        'current_page': page_info.number,
        'prev_page_url': prev_url,
        'next_page_url': next_url,
    })

