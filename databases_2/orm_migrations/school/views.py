from django.views.generic import ListView

from .models import Student


class StudentListView(ListView):
    # model = Student
    queryset = Student.objects.all().prefetch_related('teachers')
    ordering = 'group'
