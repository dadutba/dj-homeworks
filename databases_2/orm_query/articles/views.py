from django.db.models import F
from django.views.generic import ListView

from .models import Article, Genre


class ArticleListView(ListView):
    template_name = 'articles/news.html'

    # model = Article

    # queryset = Article.objects.all().select_related('genre', 'author') \
    #     .only('image', 'title', 'genre__name', 'author__name', 'text', 'published_at') \
    #     .defer('published_at')

    queryset = Article.objects.all() \
        .values('image', 'title', 'text', 'genre__name', 'author__name') \
        .annotate(genre=F('genre__name'), author=F('author__name'))
        
    ordering = '-published_at'
