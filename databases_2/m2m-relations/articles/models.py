from django.db import models


class Article(models.Model):

    title = models.CharField(max_length=256, verbose_name='Название', unique=True)
    text = models.TextField(verbose_name='Текст')
    published_at = models.DateTimeField(verbose_name='Дата публикации')
    image = models.ImageField(null=True, blank=True, verbose_name='Изображение',)

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'

    def __str__(self):
        return self.title

class Tag(models.Model):

    title = models.CharField(max_length=50, verbose_name='Название', unique=True)
    articles = models.ManyToManyField(Article, through='Membership')

    class Meta:
        verbose_name = 'Раздел'
        verbose_name_plural = 'Разделы'

    def __str__(self):
        return self.title

class Membership(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, verbose_name='Статья', related_name='tags')
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE, verbose_name='Раздел')
    main = models.BooleanField(default=False, verbose_name='Основной')

    class Meta:
        verbose_name = 'Тематика Статьи'
        verbose_name_plural = 'Тематики статей'
        unique_together = ['article', 'tag']

    def __str__(self):
        return f'{self.article}__{self.tag}'
    