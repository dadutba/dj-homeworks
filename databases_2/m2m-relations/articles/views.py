from django.db.models import Prefetch
from django.views.generic import ListView

from articles.models import Article, Membership


class ArticleListView(ListView):
    template_name = 'articles/news.html'
    # model = Article
    queryset = Article.objects.order_by('title').all().prefetch_related(Prefetch('tags', Membership.objects.order_by('-main', '-tag')))
    ordering = '-published_at'

