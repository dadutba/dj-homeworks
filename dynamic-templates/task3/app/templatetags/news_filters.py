from django import template

import re
import time


register = template.Library()

INTERVAL_10_MIN = 600
INTERVAL_24_HOURS = 86400
INTERVAL_1_HOUR = 3600

@register.filter
def format_date(value):
    difference =  time.time() - value
    if difference < INTERVAL_10_MIN:
      return 'только что'
    if difference < INTERVAL_24_HOURS:
      hours = int(difference // INTERVAL_1_HOUR)
      if hours == 1 or hours == 21:
        measure = 'час'
      elif hours > 1 and hours < 5 or hours > 21 and hours < 25:
        measure = 'часа'
      else:
        measure = 'часов' 
      return f'{hours} {measure} назад'
    return time.strftime("%Y-%m-%d", time.localtime(value))


@register.filter
def format_score(score, default_value):
    try:
       float_score = float(score)
    except ValueError:
        return default_value
    if float_score < -5:
        return 'Все плохо'
    if float_score <= 5:
        return 'Нейтрально'
    return 'Хорошо'


@register.filter
def format_num_comments(value):
    if value < 1:
      return 'Оставьте комментарий'
    elif value < 51:
      return value
    return '50+'

@register.filter
def format_selftext(str, count):
    arr = re.split(r'\s+', str)
    if len(arr) < count * 2 + 1:
        return str
    return ' '.join(arr[:count] + ['...'] + arr[-count - 1:])




