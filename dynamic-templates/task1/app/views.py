from csv import DictReader

from django.shortcuts import render
from django.views.generic import TemplateView

INFLATION_DATA_FILE = 'inflation_russia.csv'

class InflationView(TemplateView):
    template_name = 'inflation.html'

    def get(self, request, *args, **kwargs):
        with open(INFLATION_DATA_FILE, newline='', encoding='utf-8') as f:
            reader = DictReader(f, delimiter=';')
            context = { 'csv_data': reader }
            return render(request, self.template_name, context)
