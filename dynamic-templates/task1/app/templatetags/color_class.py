from django import template
register = template.Library()

@register.filter
def color_class(value):
    try:
        value = float(value)
    except ValueError:
        return ''
    if value < 0:
        return 'low'
    if value >= 5:
        return 'high3'
    if value >= 2:
        return 'high2'
    if value >= 1:
        return 'high1'
    return ''
    