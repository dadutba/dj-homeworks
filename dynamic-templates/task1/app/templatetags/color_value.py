from django import template
register = template.Library()

@register.inclusion_tag('color_value.html', takes_context=True)
def color_value(context, value, tag='td'):

    class_ = ''
    
    if not str(value).strip():
        value = '-'
    else:
        try:
            value = float(value)
            if value < 0:
                class_ = 'low'
            elif value >= 5:
                class_ = 'high3'
            elif value >= 2:
                class_ = 'high2'
            elif value >= 1:
                class_ = 'high1'
        except ValueError:
            pass

    forloop = context.get('forloop')
    if forloop:
        if forloop.get('last'):
            class_ = 'total'
        elif forloop.get('first'):
            class_ = 'first'
    
    return {
        'tag': tag,
        'value': value,
        'class_': class_
    }
