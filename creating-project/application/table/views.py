import csv

from django.shortcuts import render
from django.views.generic import TemplateView

from .models import TableDescription, TablePath

# CSV_FILENAME = 'phones.csv'
# COLUMNS = [
#     {'name': 'id', 'width': 1},
#     {'name': 'name', 'width': 3},
#     {'name': 'price', 'width': 2},
#     {'name': 'release_date', 'width': 2},
#     {'name': 'lte_exists', 'width': 1},
# ]

class TableView(TemplateView):

    template_name = 'table/table.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        csv_filename = TablePath.get_path()
        columns = TableDescription.objects.all().values()
        header = { x['order'] - 1: x['name'] for x in columns }
        with open(csv_filename, 'rt') as csv_file:
            table = []
            table_reader = csv.reader(csv_file, delimiter=';')
            next(table_reader, None)
            for table_row in table_reader:
                row = {header.get(idx) or 'col{:03d}'.format(idx): value
                        for idx, value in enumerate(table_row)}
                table.append(row)
        context.update({'columns': columns, 'table': table, 'csv_file': csv_filename})
        return context



