from django.contrib import admin

from .models import TableDescription, TablePath

admin.site.register(TableDescription)
admin.site.register(TablePath)
