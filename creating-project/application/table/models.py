from django.db import models

class TableDescription(models.Model):
    order = models.PositiveSmallIntegerField(verbose_name='Порядок', unique=True)
    name = models.CharField(max_length=40, verbose_name='Имя')
    width = models.PositiveSmallIntegerField(verbose_name='Ширина')

    def __str__(self):
        return f'{self.order}. {self.name}'

    class Meta:
        ordering = ('order', )
        verbose_name = 'Описание таблицы'
        verbose_name_plural = 'Описание таблиц'

class TablePath(models.Model):
    name = models.CharField(max_length=40, verbose_name='Имя')
    path = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name}: {self.path}'

    class Meta:
        verbose_name = 'Путь к данным'
        verbose_name_plural = 'Пути к данным'

    @staticmethod
    def get_path():
        try:
            return TablePath.objects.get(name='phone').path
        except TablePath.DoesNotExists:
            return ''

    @staticmethod
    def set_path(path):
        TablePath.objects.update_or_create(name='phone', defaults={'path': path})
