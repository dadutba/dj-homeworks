from django.db.models import Min, Max
from django.shortcuts import render
from django.views.generic import TemplateView

from .models import Station, Route

class StationsView(TemplateView):

    template_name = 'stations.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        route = self.request.GET.get('route')
        try:
            stations = Route.objects.get(name=route).stations
        except Route.DoesNotExist:
            stations = []
        try:
            if not stations:
                raise IndexError
            q = stations.aggregate(Min('latitude'), Max('latitude'), Min('longitude'), Max('longitude'))
            x = (q['longitude__min'] + q['longitude__max']) / 2
            y = (q['latitude__min'] + q['latitude__max']) / 2
        except IndexError:
            x = 37.618423
            y = 55.751244
        context.update({
            'stations': stations.all() if stations else [],
            'routes': list(Route.objects.all().values_list('name', flat=True)),
            'center': { 'x': x, 'y': y },
            'route': route
        })   
        return context
