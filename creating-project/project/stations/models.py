from django.db import models

class Route(models.Model):
    name = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.name


class Station(models.Model):
    name = models.CharField(max_length=40)
    latitude = models.FloatField()
    longitude = models.FloatField()
    routes = models.ManyToManyField(Route, related_name='stations')

    def __str__(self):
        return self.name


