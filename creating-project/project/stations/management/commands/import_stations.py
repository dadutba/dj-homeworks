import csv
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from stations.models import Station, Route

class Command(BaseCommand):
    help = 'Загрузить начальные данные по остановкам и маршрутам в БД'
    requires_migrations_checks = True

    def handle(self, *args, **options):

        ThroughModel = Station.routes.through

        if connection.vendor == 'sqlite':
            station_batch_size = 999 // len(Station._meta.fields)
            route_batch_size = 999 // len(Route._meta.fields)
            through_batch_size = 999 // len(ThroughModel._meta.fields) 
        else:
            station_batch_size = None
            route_batch_size = None
            through_batch_size = None

        with open(settings.BUS_STATION_CSV, encoding='cp1251') as f:
            reader = csv.DictReader(f, delimiter=';')

            Station.objects.all().delete()
            Route.objects.all().delete()

            throughs = []
            stations = []
            routes = {}
            j = 0

            for i, row in enumerate(reader):
                station = Station(
                    id = i,
                    name=row.get('Name'),
                    latitude=row.get('Latitude_WGS84'),
                    longitude=row.get('Longitude_WGS84'),
                )
                stations.append(station)
                for name in row.get('RouteNumbers').split(';'):
                    name = name.strip()
                    route = routes.setdefault(name, Route(id=j, name=name))
                    if route.id == j:
                        j += 1
                    throughs.append(ThroughModel(station_id=i, route_id=route.id))

            Station.objects.bulk_create(stations, station_batch_size)
            Route.objects.bulk_create(routes.values(), route_batch_size)
            ThroughModel.objects.bulk_create(throughs, through_batch_size)


            # for row in reader:
            #     station = Station.objects.create(
            #         name=row.get('Name'),
            #         latitude=row.get('Latitude_WGS84'),
            #         longitude=row.get('Longitude_WGS84'),
            #     )
            #     for name in row.get('RouteNumbers').split(';'):
            #         route, _ = Route.objects.get_or_create(name=name)
            #         station.routes.add(route)

